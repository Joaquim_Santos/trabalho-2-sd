package sistemaFigura;

import java.net.MalformedURLException;
import java.rmi.RemoteException; 
import java.rmi.Naming; //Armazena e obtem referências de objetos remotos em um registro de objeto remoto


public class Servidor{

    public Servidor() {}
        
    public static void main(String[] args){
        try {
            //Instância da classe que implementa a interface
            Interface obj = new Interface(); 
            Naming.rebind("servidorfiguras",obj);
            System.err.println("O servidor está ligado!");
            
        } catch(MalformedURLException | RemoteException e){
            System.err.println("Erro no servidor: " + e.toString()); 
        } 
    }
}