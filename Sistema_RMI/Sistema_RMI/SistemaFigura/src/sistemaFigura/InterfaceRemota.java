package sistemaFigura;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceRemota extends Remote {

    public String figPCliente() throws RemoteException;
}