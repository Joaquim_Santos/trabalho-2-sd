package sistemaFigura;

import javax.swing.*;
import java.rmi.*;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cliente extends JFrame{

    int numRequisicoes=0, contaRequisicoes=0;
    InterfaceRemota iRemota;
    FiguraGeometrica figura;
    
    public Cliente() {
        initComponents();
        try{
            String url = "rmi://localhost/servidorfiguras";
            iRemota =(InterfaceRemota) Naming.lookup(url);
            
            /* Condicões iniciais */
            jButton1.setEnabled(false);
            jLabel2.setIcon(null);
            jLabel2.setText("Aguardando nova busca!");
            contaRequisicoes = 0;
            numRequisicoes = 0;
        }
        catch(MalformedURLException | NotBoundException | RemoteException e){
            System.err.println("Erro no Cliente: " + e.toString()); 
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBackground(new java.awt.Color(3, 28, 84));
        jPanel1.setLayout(null);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Noto Sans", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(jLabel2);
        jLabel2.setBounds(40, 70, 350, 240);

        jLabel5.setBackground(new java.awt.Color(3, 28, 84));
        jLabel5.setFont(new java.awt.Font("Noto Sans", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Entre com o número de figuras");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(0, 20, 430, 26);

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setForeground(new java.awt.Color(3, 28, 84));
        jButton1.setText("Próxima figura");
        jButton1.setToolTipText("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(140, 320, 150, 31);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(20, 20, 430, 370);

        jPanel2.setBackground(new java.awt.Color(3, 28, 84));

        jLabel3.setFont(new java.awt.Font("Noto Sans", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Entre com o número de figuras");

        jTextField4.setBackground(new java.awt.Color(138, 138, 138));
        jTextField4.setForeground(new java.awt.Color(255, 255, 255));
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField4.setCaretColor(new java.awt.Color(0, 89, 255));
        jTextField4.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTextField4.setName("Entrada Numero Requisicoes"); // NOI18N
        jTextField4.setOpaque(false);
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(255, 255, 255));
        jButton2.setForeground(new java.awt.Color(3, 28, 84));
        jButton2.setText("Buscar figuras");
        jButton2.setToolTipText("");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Noto Sans", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Sistema Busca Figuras");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(470, 130, 210, 170);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon("/home/pauloh/Desktop/Trabalho Prático 2/background.jpg")); // NOI18N
        jLabel1.setLabelFor(jLabel1);
        jLabel1.setText("Background");
        jLabel1.setToolTipText("");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 720, 410);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        //Entrada do usuário
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        String entradaNumRequisicoes = jTextField4.getText();
        System.out.println("Numero de requisicoes: "+entradaNumRequisicoes);
        numRequisicoes = Integer.parseInt(entradaNumRequisicoes);
        
        /* Condicões iniciais */
        jButton1.setEnabled(false);
        jLabel2.setIcon(null);
        jLabel2.setText("Aguardando nova busca!");
        contaRequisicoes = 0;
            
        if(numRequisicoes > 0){
            try {
                figura = new FiguraGeometrica(iRemota.figPCliente());
            } catch (RemoteException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Primeira figura
            contaRequisicoes++;
            imprimeFigura(figura, contaRequisicoes);
            if(numRequisicoes > 1){
                jButton1.setEnabled(true);
                jButton2.setEnabled(false); //Desativa BuscaFiguras
            }
            else if(numRequisicoes <= 1){
                jLabel5.setText("Game over!");
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        //Demais figuras
        try {
            //Próxima figura
            figura = new FiguraGeometrica(iRemota.figPCliente());
        } catch (RemoteException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        contaRequisicoes++;
        if(contaRequisicoes <= numRequisicoes){
            imprimeFigura(figura, contaRequisicoes);
        }
        else if(contaRequisicoes > numRequisicoes){
            jTextField4.setText(null);
            jButton1.setEnabled(false);
            jButton2.setEnabled(true); //Ativa BuscaFiguras
            jLabel2.setIcon(null);
            jLabel5.setText("Você só pediu "+numRequisicoes+" figuras");
            jLabel2.setText("Aguardando nova busca!");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
    
    public void imprimeFigura(FiguraGeometrica figura, int i){
        
        ImageIcon imagem;
        
        if(figura.getNome().equalsIgnoreCase("triangulo")){
            jLabel5.setText("A figura "+(i)+" é um Triangulo!");
            imagem = new ImageIcon(getClass().getResource("triangulo.png"));
        }
        else if(figura.getNome().equalsIgnoreCase("retangulo")){
            jLabel5.setText("A figura "+(i)+" é um Retângulo!");
            imagem = new ImageIcon(getClass().getResource("retangulo.jpg"));
        }
        else if(figura.getNome().equalsIgnoreCase("quadrado")){
            jLabel5.setText("A figura "+(i)+" é um Quadrado!");
            imagem = new ImageIcon(getClass().getResource("quadrado.jpg"));
        }
        else {
            jLabel5.setText("A figura "+(i)+" é um Circulo!");
            imagem = new ImageIcon(getClass().getResource("circulo.png"));
        }
        jLabel2.setText(null);
        jLabel2.setIcon(imagem);
    }
    
    public static void main(String args[]) {
        
        /*Cria e exibe a interface gráfica */
        java.awt.EventQueue.invokeLater(() -> {new Cliente().setVisible(true);});
    }
}