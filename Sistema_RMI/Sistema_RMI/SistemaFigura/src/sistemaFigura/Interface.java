package sistemaFigura;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject; 
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Interface extends UnicastRemoteObject implements InterfaceRemota{

    int numRequisicoes;
    List <FiguraGeometrica> figuras = new ArrayList<> ();
    
    Interface() throws RemoteException{}  
    
    //Construção das possibilidades de figuras
    public void DefineFiguras(){
        
        FiguraGeometrica figura;
        figura = new FiguraGeometrica("quadrado");
        figuras.add(figura);
        
        figura = new FiguraGeometrica("retangulo");
        figuras.add(figura);
        
        figura = new FiguraGeometrica("circulo");
        figuras.add(figura);
        
        figura = new FiguraGeometrica("triangulo");
        figuras.add(figura);
    }
    
    @Override
    public String figPCliente(){
                
        int numFigura;
        Random randFig = new Random();
        
        if(figuras.isEmpty()){
            DefineFiguras();
        } else{
            figuras.clear();
            DefineFiguras();
        }

        numFigura = randFig.nextInt(figuras.size());
        System.out.println("A figura escolhida foi: "+figuras.get(numFigura).getNome());
        
        return figuras.get(numFigura).getNome();
    }
}