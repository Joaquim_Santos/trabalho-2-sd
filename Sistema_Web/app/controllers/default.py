# -*- coding: utf-8 -*-
from app import app
from flask import render_template, flash, request
import random
from flask import jsonify

figuras_geometricas = ['quadrado', 'retangulo', 'circulo', 'triangulo']

@app.route("/", methods=['GET', 'POST'])
@app.route("/home", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        #Escolhe-se aleatoriamente uma figura para enviar ao cliente
        figura_escolhida = random.choice(figuras_geometricas)
        print(figura_escolhida)
        return jsonify(figura_escolhida)
    else:
        return render_template('index.html', page_title= "Home")