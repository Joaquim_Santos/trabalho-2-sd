# -*- coding: utf-8 -*-
import pygame

#Definições dos objetos
pygame.init()
tela = pygame.display.set_mode([1180, 700]) #Definição da tela a ser exibida
pygame.display.set_caption("Figuras retornadas pelo servidor")
relogio = pygame.time.Clock() #marcação de tempo para atualizar

#Cores RGB
azul_claro = (0,127,255)
magenta  = (255, 0, 255)
verde = (0,255,0)
lista_cores = []
bright_ouro  = (217,217,25)
branco = (255,255,255)
preto = (0,0,0)
madeira = (165,128,100)
vermelho = (255,0,0)
chocolate = (92,51,23)
lista_cores.append(bright_ouro)
lista_cores.append(branco)
lista_cores.append(preto)
lista_cores.append(madeira)
lista_cores.append(azul_claro)
lista_cores.append(vermelho)

#definição de tamanho e cor da superfície a ser desenhada sobre a tela
superficie = pygame.Surface((1080, 600))
superficie.fill(azul_claro)
#definições das figuras a serem desenhadas
#quadrado = pygame.Rect(380, 80, 300, 300)
#retangulo = pygame.Rect(340, 80, 400, 270)

#Definindo fontes para palavras a serem escritas na tela
pygame.font.init()
fonte = pygame.font.get_default_font()
fonte_texto = pygame.font.SysFont(fonte, 30)
texto = fonte_texto.render('Foram recebidas as seguintes figuras do Servidor:', 1, chocolate)
texto2 = fonte_texto.render('Teclas direcionais esquerda e direita - trocar cor de fundo', 1, chocolate)
texto4 = fonte_texto.render('Enter - Play/Pause  áudio', 1, chocolate)

#Seleção de arquivo de áudio para reprodução
audio_sonic = pygame.mixer.music
audio_sonic.load('sonic2006.ogg')
audio_sonic.play(loops=-1)
playing = True

i = 0
cor = lista_cores[i]
sair = False
figura = ''

while (sair == False):
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            sair = True
        if evento.type == pygame.KEYDOWN: #verifica se uma tecla foi pressionada
            #Cada if verifica qual tecla direcional foi  pressionada. DOis primeiros if são para alterar a cor da figura
            if evento.key == pygame.K_LEFT:
                if(i == 0):
                    i = 5
                else:
                    i = i - 1

            elif evento.key == pygame.K_RIGHT:
                if(i == 5):
                    i = 0
                else:
                    i = i + 1


            elif evento.key == pygame.K_RETURN:
                if playing:
                    audio_sonic.pause()
                    playing = False
                else:
                    audio_sonic.unpause()
                    playing = True

            cor = lista_cores[i]

    #Atualizar a tela com 27 Frames por segundo
    relogio.tick(27)
    tela.fill(verde)
    tela.blit(superficie, [50, 50]) #Chamando a superfície. ùltimos parâmetros são a posição no eixo x e y da superfície
    superficie.fill(cor)
    superficie.blit(texto, (300, 10)) #Insere o objeto texto criado na posição especificada para a superfície
    superficie.blit(texto2, (80, 410))
    superficie.blit(texto4, (80, 490))

    #Verifica qual a figura escolhida e faz o desenho correspondente na superfície feita anteriormente (função draw desenha cada figura)
    if(figura == 'quadrado'):
        #pygame.draw.rect(superficie, verde, quadrado)
        imagem = pygame.image.load('quadrado.jpg')
        superficie.blit(imagem, (0, 0))
    elif (figura == 'retângulo'):
        #pygame.draw.rect(superficie, verde, retangulo)
        imagem = pygame.image.load('retangulo.jpg')
        superficie.blit(imagem, (0, 0))
    elif (figura == 'círculo'):
        #pygame.draw.circle(superficie, verde, (540,230), 150)
        imagem = pygame.image.load('circulo.png')
        superficie.blit(imagem, (0, 0))
    else:
        #pygame.draw.polygon(superficie, verde,  [[550, 80], [400, 380], [700, 380]])
        imagem = pygame.image.load('triangulo.png')
        superficie.blit(imagem, (0, 0))

    pygame.display.update()

pygame.quit()
