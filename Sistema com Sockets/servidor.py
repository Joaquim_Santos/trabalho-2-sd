# -*- coding: utf-8 -*-
import random
from socket import *

figuras_geometricas = ['quadrado', 'retângulo', 'círculo', 'triângulo']

#endereço do servidor é o localhost
endereco_servidor = ''

#Número de porta não padrão
porta = 50007

#Criação do objeto socket. Primeiro parâmetro é a família do endereço(IP). Sehundo diz que é protocolo de transferência TCP
#Combinação destes dois parAetros indica que é um servidor TCP/IP
servidor_socket = socket(AF_INET, SOCK_STREAM)

#vuncular servidor ao endereço e à porta
servidor_socket.bind((endereco_servidor, porta))

#Servidor começa a esperar pelos clientes. No máximo são 3
servidor_socket.listen(3)

while(True):
    #Aceita uma conexão quando encontrada e devolve um novo socket que representa a conexão, junto do endereço do cliente conectado
    conexao, endereco = servidor_socket.accept()
    print("Servidor conectado com "+ str(endereco))

    while(True):
        #Recebe uma mensagem enviada pelo cliente, tamanho de 1024 bytes
        mensagem = conexao.recv(1024)

        #Decodificação da mensagem
        mensagem = mensagem.decode('utf-8')
        print("Mensagem recebida do cliente: " + mensagem)

        #Quebra-se o loop se nada for recebido
        if not mensagem:
            break

        #Escolhe-se aleatoriamente uma figura para enviar ao cliente
        figura_escolhida = random.choice(figuras_geometricas)
        print("Figura escolhida para enviar ao cliente: " + figura_escolhida)

        #Servidor envia uma resposta ao cliente. Mensqagem é codificada em bytes
        mensagem = figura_escolhida.encode('utf-8')
        conexao.send(mensagem)

    #Fecha conexão após parar de receber dados
    conexao.close()




